import "./App.css";

function App() {
  return (
    <div className="App">
      <iframe
        src="https://app.skand.io/explore/share/6136b8915d32ab004d7ac635/6136b8915d32ab004d7ac636"
        style={{
          position: "absolute",
          top: "0",
          left: "0",
          width: "100%",
          height: "100%",
        }}
        title="New Timeline"
      />
    </div>
  );
}

export default App;
